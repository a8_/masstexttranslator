package me.a8;


import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Main {
    static String text ="";
    static final String[] Langs = {"az", "sq", "am", "en", "ar", "hy", "af", "eu", "ba", "be", "bn", "my", "bg", "bs", "cy", "hu", "vi", "ht", "gl", "nl", "mrj", "el", "ka", "gu", "da", "he", "yi", "id", "ga", "it", "is", "es", "kk", "kn", "ca", "ky", "zh", "ko", "xh", "km", "lo", "la", "lv", "lt", "lb", "mg", "ms", "ml", "mt", "mk", "mi", "mr", "mhr", "mn", "de", "ne", "no", "pa", "pap", "fa", "pl", "pt", "ro", "ru", "ceb", "sr", "si", "sk", "sl", "sw", "su", "tg", "th", "tl", "ta", "tt", "te", "tr", "udm", "uz", "uk", "ur", "fi", "fr", "hi", "hr", "cs", "sv", "gd", "et", "eo", "jv", "ja"};

    public static void main(String[] args) throws UnsupportedEncodingException {
        System.out.println("MassTextTranslator  Copyright (C) 2020  a8_");
        System.out.println("This program comes with ABSOLUTELY NO WARRANTY;");
        System.out.println("This is free software, and you are welcome to redistribute it");
        System.out.println("Powered by Yandex.Translate");

        System.out.println();
        System.out.println("Type the text you want to translate");
        Scanner scanner = new Scanner(System.in);
        text = scanner.nextLine();
        text = URLEncoder.encode(text, StandardCharsets.UTF_8.toString());
        System.out.println();
        System.out.println("How much languages do you want to translate through? max is 93");
        int langnum = scanner.nextInt();
        scanner.close();


        for (int i = 0; i < langnum; i++) {

            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder().uri(URI.create("https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20200423T022424Z.3abcec388fbd9e5f.173bede626a1c7c719263789c15bff46980be543&text=" + text + "&lang=" + Langs[i])).build();
            client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(HttpResponse::body)
                    .thenApply(Main::parse)
                    .join();

            text = URLEncoder.encode(text, StandardCharsets.UTF_8.toString());

            int b = i + 1;
            System.out.println("Did " + b + " languages so far");
        }






        text = URLEncoder.encode(text, StandardCharsets.UTF_8.toString());
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest  request = HttpRequest.newBuilder().uri(URI.create("https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20200423T022424Z.3abcec388fbd9e5f.173bede626a1c7c719263789c15bff46980be543&text=" + text + "&lang=en")).build();
             client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApply(HttpResponse::body)
                    .thenApply(Main::parse)
                    .join();

        text = URLDecoder.decode(text, StandardCharsets.UTF_8.toString());

        System.out.println();
            System.out.println(text);







    }
    public static String parse(String responsebody){
        JSONObject jsonObject = new JSONObject(responsebody);
        String textJ = jsonObject.getJSONArray("text").getString(0);

        text = textJ;
        return null;

    }


}
